# Machine Week

*Machines in a week*  
*It's easy, so to speak*   
*In minutia is mayhem*  

[Machine Week 2017](http://gitlab.cba.mit.edu/jakeread/machineweek-2017)  
[Machine Week 2018](http://gitlab.cba.mit.edu/jakeread/machineweek-2018)  
[Machine Week 2019](https://gitlab.cba.mit.edu/jakeread/machineweek-2019)  
[Machine Week 2020: every week is machine week](https://gitlab.cba.mit.edu/jakeread/clank-lz)  
